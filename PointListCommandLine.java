import java.awt.*;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;

public class PointListCommandLine extends ArrayPointList {

    public static void main(String[] args) throws IOException {

        InputStreamReader streamReader = new InputStreamReader(System.in);
        StreamTokenizer streamTokenizer = new StreamTokenizer(streamReader);
        PointList pointlist = new ArrayPointList();
        Point point;

        while (true) {
            if (streamTokenizer.nextToken() == StreamTokenizer.TT_WORD)

                switch (streamTokenizer.sval) {

                    case "next":
                        System.out.println(pointlist.goToNext());
                        break;


                    case "start":
                        System.out.println(pointlist.goToBeginning());
                        break;

                    case "prev":
                        System.out.println(pointlist.goToPrior());
                        break;

                    case "empty":
                        System.out.println(pointlist.isEmpty());
                        break;

                    case "end":
                        System.out.println(pointlist.goToEnd());
                        break;

                    case "clear":
                        pointlist.clear();
                        break;

                    case "full":
                        System.out.println(pointlist.isFull());
                        break;

                    case "curr":
                        System.out.println("(" + pointlist.getCursor().x + ", " + pointlist.getCursor().y + ")");
                        break;

                    case "quit":
                        System.exit(1);
                        break;

                    case "add":
                        point = new Point();
                        streamTokenizer.nextToken();

                        point.x = (int) streamTokenizer.nval;
                        streamTokenizer.nextToken();

                        point.y = (int) streamTokenizer.nval;
                        pointlist.append(point);
                        break;

                    default:
                        break;
                }
        }
    }
}
