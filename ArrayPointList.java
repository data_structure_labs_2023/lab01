import java.awt.*;
import java.util.Arrays;

public class ArrayPointList implements PointList {

    private final Point[] my_array;
    private int cnt;
    private int curser;
    private final int my_max_size;
    //Default Constractor
    ArrayPointList(){
        my_array = new Point[MAX_SIZE];
        curser =-1;
        cnt = 0;
        my_max_size = MAX_SIZE;
    }
    //Constractor how gets only one arrgument
    ArrayPointList(int x) {
        my_array = new Point[x];
        curser = -1;
        cnt = 0;
        my_max_size = x;
    }

    @Override
    public void append(Point newPoint) {
        curser++;
        my_array[curser] = new Point(newPoint);
        cnt++;
    }

    @Override
    public void clear() {
        cnt=0;
        curser=-1;
    }

    @Override
    public boolean isEmpty() {
        return cnt == 0;
    }

    @Override
    public boolean isFull() {
        return cnt == my_max_size;
    }

    @Override
    public boolean goToBeginning() {
        if(isEmpty()){
            return false;
        }
        curser = 0;
        return true;
    }

    @Override
    public boolean goToEnd() {
        if(isEmpty()){
            return false;
        }
        curser = cnt-1;
        return true;
    }

    @Override
    public boolean goToNext() {
        if(curser+1 == cnt){
            return false;
        }
        ++curser;
        return true;
    }

    @Override
    public boolean goToPrior() {
        if(curser+1 == 0){
            return false;
        }
        --curser;
        return true;
    }

    @Override
    public Point getCursor() {
        if(isEmpty()){
            return  null;
        }
        return my_array[curser];
    }

    @Override
    public String toString() {
        return "ArrayPointList{" + "my_array=" + Arrays.toString(my_array) + '}';
    }
}
